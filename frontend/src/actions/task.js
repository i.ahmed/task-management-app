import requests from "./request";

const getToken = () => {
  const { token } = JSON.parse(localStorage.getItem("currentUser"));
  return token;
};

const getTasks = async (setTasks) => {
  const token = getToken();
  const { data } = await requests({}, "/api/tasks", "GET", {
    Authorization: `Bearer ${token}`,
  });
  setTasks(data);
};

const getTask = async (id, setIsError, setErrors) => {
  const token = getToken();
  const { data, error } = await requests({}, `/api/tasks/${id}`, "GET", {
    Authorization: `Bearer ${token}`,
  });
  if (!error) {
    setIsError(false);
    setErrors([]);
    return data;
  }
  setIsError(true);
  setErrors([{ text: error.error }]);
  return { _id: "", description: "", completed: false };
};

const addTask = async (payload, setIsError, setErrors, setTask) => {
  const token = getToken();
  payload["completed"] = payload["completed"] === "completed" ? true : false;
  const { error } = await requests(payload, "/api/tasks", "POST", {
    Authorization: `Bearer ${token}`,
  });
  if (!error) {
    setIsError(false);
    setTask({ description: "", completed: "not-completed" });
    alert("Task Added Successfully :>");
    return setErrors([]);
  }
  setIsError(true);
  setErrors([{ text: error.error }]);
};

const updateTask = async (payload, id, setIsError, setErrors) => {
  const token = getToken();
  payload["completed"] = payload["completed"] === "completed" ? true : false;
  const { error } = await requests(payload, `/api/tasks/${id}`, "PATCH", {
    Authorization: `Bearer ${token}`,
  });
  if (!error) {
    setIsError(false);
    alert("Task Updated Successfully :>");
    return setErrors([]);
  }
  setIsError(true);
  setErrors([{ text: error.error }]);
};

const deleteTask = async (id, setTasks) => {
  const token = getToken();
  if (window.confirm("Are You Sure !")) {
    const { error } = await requests({}, `/api/tasks/${id}`, "DELETE", {
      Authorization: `Bearer ${token}`,
    });
    if (!error) {
      getTasks(setTasks);
      return alert("Task Deleted Successfully :>");
    }
    alert(`There Is And Error : ${error.error} :<`);
  }
};
export { getToken, getTasks, getTask, addTask, updateTask, deleteTask };
