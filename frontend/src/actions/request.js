import axios from "axios";

const requests = async (data, route, method, headers = {}) => {
  try {
    let config = { method, url: route, data, withCredentials: true };
    if (headers) {
      config["headers"] = headers;
    }
    console.log(config);
    const response = await axios(config);
    return { status: response.status, data: response.data };
  } catch (e) {
    return { status: e.response.status, error: e.response.data };
  }
};

export default requests;
