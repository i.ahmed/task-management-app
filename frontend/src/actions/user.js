import requests from "./request";

const signup = async (
  user,
  history,
  route,
  setCurrentUser,
  setIsError,
  setErrors
) => {
  const { data, error } = await requests(user, route, "POST");
  if (!error) {
    const {
      user: { _id },
      token,
    } = data;
    setIsError(false);
    setErrors([]);
    setCurrentUser({ _id, token });
    localStorage.setItem("currentUser", JSON.stringify({ _id, token }));
    history.push("/tasks");
  } else {
    setErrors([{ text: error.error }]);
    setIsError(true);
  }
};

const login = async (
  user,
  history,
  route,
  setCurrentUser,
  setIsError,
  setErrors
) => {
  const { data, error } = await requests(user, route, "POST");
  if (!error) {
    const {
      user: { _id },
      token,
    } = data;
    setIsError(false);
    setErrors([]);
    setCurrentUser({ _id, token });
    localStorage.setItem("currentUser", JSON.stringify({ _id, token }));
    history.push("/tasks");
  } else {
    setErrors([{ text: error.error }]);
    setIsError(true);
  }
};

const logout = async () => {
  const { token } = JSON.parse(localStorage.getItem("currentUser"));
  const { error } = await requests({}, `/api/users/logout`, "POST", {
    Authorization: `Bearer ${token}`,
  });
  if (!error) {
    localStorage.removeItem("currentUser");
    window.location.replace("/");
  } else {
    alert(`There Is Some Think Wrong`);
  }
};

export { login, signup, logout };
