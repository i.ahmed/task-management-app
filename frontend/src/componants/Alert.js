import React from "react";

const Alert = ({ messages, status }) => {
  return (
    <section className={`alert ${status}-alert`}>
      {status === "error" ? (
        <>
          <h4>Errors : </h4>
          <ul>
            {messages.map((message, index) => {
              const { text } = message;
              return <li key={index}>{text}</li>;
            })}
          </ul>
        </>
      ) : (
        <h4>{messages[0].text}</h4>
      )}
    </section>
  );
};

export default Alert;
