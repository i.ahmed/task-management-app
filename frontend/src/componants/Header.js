import React from "react";
import { useHistory } from "react-router-dom";
import { useGlobalContext } from "../context";

const Header = () => {
  const history = useHistory();
  const { logout } = useGlobalContext();
  return (
    <nav>
      <div className="brand space">
        <h2>Tasks</h2>
      </div>
      <ul className="space">
        <li className="space" onClick={() => logout(history)}>
          logout
        </li>
      </ul>
    </nav>
  );
};

export default Header;
