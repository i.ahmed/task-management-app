import React, { useState, useEffect } from "react";
import { FaFacebookF, FaGooglePlusG } from "react-icons/fa";
import { Link, useHistory } from "react-router-dom";
import { LoginSocialFacebook, LoginSocialGoogle } from "reactjs-social-login";
import { useGlobalContext } from "../context";
import { userFormValidation } from "../validations/user-validations";
import { google, facebook } from "../config/keys";
import Alert from "./Alert";

const SignUp = () => {
  const [user, setUser] = useState({
    name: "",
    email: "",
    username: "",
    password: "",
  });
  const { signup, setCurrentUser, isError, setIsError, errors, setErrors } =
    useGlobalContext();
  const history = useHistory();
  const handleChange = (e) => {
    const key = e.target.name;
    const value = e.target.value;
    setUser({
      ...user,
      [key]: value,
    });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setErrors(userFormValidation(user));
    if (errors.length === 0) {
      setIsError(false);
      setErrors([]);
      return signup(
        user,
        history,
        "/api/users",
        setCurrentUser,
        setIsError,
        setErrors
      );
    }
    setIsError(true);
  };
  useEffect(() => {
    setIsError(false);
    setErrors([]);
  }, []);
  return (
    <div className="wrapper">
      <div className="login-wrapper">
        <form onSubmit={handleSubmit}>
          {isError && <Alert messages={errors} status="error" />}
          <h3>Sign Up Manually</h3>
          <input
            type="text"
            name="name"
            placeholder="NAME"
            required
            value={user.name}
            onChange={(e) => handleChange(e)}
          />
          <input
            type="text"
            name="email"
            placeholder="EMAIL"
            required
            value={user.email}
            onChange={(e) => handleChange(e)}
          />
          <input
            type="text"
            name="username"
            placeholder="USERNAME"
            required
            value={user.username}
            onChange={(e) => handleChange(e)}
          />
          <input
            type="password"
            name="password"
            placeholder="PASSWORD"
            required
            value={user.password}
            onChange={(e) => handleChange(e)}
          />
          <button>sign up</button>
          <span>
            Already Have Account Login <Link to="/">here</Link>
          </span>
        </form>
        <div className="spliter">
          <span>OR</span>
        </div>
        <div className="social-wrapper">
          <LoginSocialFacebook
            appId={facebook.appId}
            onResolve={(response) => {
              const { email, name } = response.data;
              signup(
                { email, name, provider: "facebook" },
                history,
                "/api/users",
                setCurrentUser,
                setIsError,
                setErrors
              );
            }}
            onReject={(err) => console.log(err)}
            className="facebook"
          >
            <FaFacebookF style={{ fontSize: "1.3rem" }} />
            <span>Sign Up with facebook</span>
          </LoginSocialFacebook>
          <LoginSocialGoogle
            client_id={google.clientID}
            scope="email profile"
            onResolve={(response) => {
              const {
                gt: { Rt: email, Te: name },
              } = response.data;
              signup(
                { email, name, provider: "google" },
                history,
                "/api/users",
                setCurrentUser,
                setIsError,
                setErrors
              );
            }}
            onReject={() => alert("Opps! Some Think Went Wronge")}
            className="google"
          >
            <FaGooglePlusG style={{ fontSize: "1.3rem" }} />
            <span>Sign Up with google</span>
          </LoginSocialGoogle>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
