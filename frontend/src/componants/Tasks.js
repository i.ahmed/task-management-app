import React from "react";
import Header from "./Header";
import TasksList from "./TasksList";
const Tasks = () => {
  return (
    <section className="main">
      <Header />
      <TasksList />
    </section>
  );
};

export default Tasks;
