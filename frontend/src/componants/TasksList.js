import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useGlobalContext } from "../context";
import SingleTask from "./SingleTask";
import Alert from "./Alert";

const TasksList = () => {
  const { getTasks, setTasks, tasks } = useGlobalContext();
  useEffect(() => {
    getTasks(setTasks);
  }, []);
  return (
    <section className="tasks-section space">
      <div className="button-wrapper">
        <Link to="/task/add" className="button">
          new task
        </Link>
      </div>
      {tasks.length > 0 ? (
        <section className="list">
          {tasks.map((task) => (
            <SingleTask key={task._id} {...task} />
          ))}
        </section>
      ) : (
        <Alert messages={[{ text: "No Tasks Found" }]} status="info" />
      )}
    </section>
  );
};

export default TasksList;
