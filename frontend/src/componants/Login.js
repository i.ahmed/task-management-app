import React, { useState, useEffect } from "react";
import { FaFacebookF, FaGooglePlusG } from "react-icons/fa";
import { Link, useHistory } from "react-router-dom";
import { LoginSocialFacebook, LoginSocialGoogle } from "reactjs-social-login";
import { useGlobalContext } from "../context";
import { userFormValidation } from "../validations/user-validations";
import { google, facebook } from "../config/keys";
import Alert from "./Alert";

const Login = () => {
  const [user, setUser] = useState({ email: "", password: "" });
  const { login, setCurrentUser, isError, setIsError, errors, setErrors } =
    useGlobalContext();
  const history = useHistory();
  const handleChange = (e) => {
    const key = e.target.name;
    const value = e.target.value;
    setUser({
      ...user,
      [key]: value,
    });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setErrors(userFormValidation(user));
    if (errors.length === 0) {
      setIsError(false);
      setErrors([]);
      return login(
        user,
        history,
        "/api/users/login",
        setCurrentUser,
        setIsError,
        setErrors
      );
    }
  };

  useEffect(() => {
    setIsError(false);
    setErrors([]);
  }, []);
  return (
    <div className="wrapper">
      <div className="login-wrapper">
        <form onSubmit={handleSubmit}>
          {isError && <Alert messages={errors} status="error" />}
          <h3>Sign In Manually</h3>
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={user.email}
            onChange={handleChange}
            required
          />
          <input
            type="password"
            name="password"
            placeholder="PASSWORD"
            value={user.password}
            onChange={handleChange}
            required
          />
          <button>Login</button>
          <span>
            Don't have Account Make One <Link to="/signup">here</Link>
          </span>
        </form>
        <div className="spliter">
          <span>OR</span>
        </div>
        <div className="social-wrapper">
          <LoginSocialFacebook
            appId={facebook.appId}
            onResolve={(response) => {
              const { email } = response.data;
              login(
                { email },
                history,
                "/api/users/login/facebook",
                setCurrentUser,
                setIsError,
                setErrors
              );
            }}
            onReject={(err) => console.log(err)}
            className="facebook"
          >
            <FaFacebookF style={{ fontSize: "1.3rem" }} />
            <span>Sign Up with facebook</span>
          </LoginSocialFacebook>
          <LoginSocialGoogle
            client_id={google.clientID}
            onResolve={(response) => {
              const {
                gt: { Rt: email },
              } = response.data;
              login(
                { email },
                history,
                "/api/users/login/google",
                setCurrentUser,
                setIsError,
                setErrors
              );
            }}
            onReject={(err) => console.log(err)}
            className="google"
          >
            <FaGooglePlusG style={{ fontSize: "1.3rem" }} />
            <span>Sign Up with google</span>
          </LoginSocialGoogle>
        </div>
      </div>
    </div>
  );
};

export default Login;
