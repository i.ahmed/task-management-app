import React from "react";
import { RiEditBoxLine, RiDeleteBin7Line } from "react-icons/ri";
import { Link } from "react-router-dom";
import { useGlobalContext } from "../context";
const SingleTask = ({ description, completed, _id }) => {
  const { deleteTask, setTasks } = useGlobalContext();
  return (
    <article className="item">
      <header>
        <h4>{description}</h4>
        <div className="icons-container">
          <Link to={`/task/${_id}/edit`}>
            <RiEditBoxLine color="blue" size="1.5rem" />
          </Link>
          <RiDeleteBin7Line
            color="red"
            size="1.5rem"
            onClick={() => deleteTask(_id, setTasks)}
          />
        </div>
      </header>
      <p className={`${completed ? "success" : "normal"}`}>
        {completed ? "Completed" : "Not-Completed"}
      </p>
    </article>
  );
};

export default SingleTask;
