import React, { useEffect, useState } from "react";
import { useGlobalContext } from "../context";
import Alert from "./Alert";
import { taskValidation } from "../validations/task-validations";
const TaskForm = ({ match: { params } }) => {
  const {
    getTask,
    addTask,
    updateTask,
    isError,
    errors,
    setIsError,
    setErrors,
  } = useGlobalContext();
  const [task, setTask] = useState({
    description: "",
    completed: "not-completed",
  });

  const changeHandler = (e) => {
    const key = e.target.name;
    let value = e.target.value;
    setTask({ ...task, [key]: value });
    console.log(key, typeof value);
  };
  const submitHandler = (e) => {
    e.preventDefault();
    const errors = taskValidation(task);
    if (errors.length === 0) {
      setIsError(false);
      setErrors([]);
      if (params.id) {
        alert(JSON.stringify(task));
        return updateTask(task, params.id, setIsError, setErrors);
      }
      return addTask(task, setIsError, setErrors, setTask);
    }
    setIsError(true);
    setErrors(errors);
  };
  useEffect(() => {
    async function fetchData() {
      if (params.id) {
        const oldTask = await getTask(params.id, setIsError, setErrors);
        console.log(oldTask);
        setTask({
          ...task,
          description: oldTask.description,
          completed: oldTask.completed ? "completed" : "not-completed",
        });
      }
    }
    fetchData();
  }, []);
  return (
    <div className="wrapper task">
      <div className="login-wrapper">
        <form onSubmit={(e) => submitHandler(e)}>
          {isError && <Alert messages={errors} status="error" />}
          <h3>Task</h3>
          <select
            name="completed"
            value={task.completed}
            onChange={changeHandler}
          >
            <option value="completed">Completed</option>
            <option value="not-completed">Not Compeleted</option>
          </select>
          <input
            placeholder="Description"
            name="description"
            value={task.description}
            onChange={changeHandler}
          />
          <button>{params.id ? "update task" : "add task"}</button>
        </form>
      </div>
    </div>
  );
};

export default TaskForm;
