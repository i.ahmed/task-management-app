import validator from "validator";

const userFormValidation = (user) => {
  let errorMessages = [];
  for (let key in user) {
    if (validator.isEmpty(user[key])) {
      errorMessages.push({ field: key, text: `The ${key} Is Empty` });
    }
  }
  if (!validator.isEmail(user.email)) {
    errorMessages.push({
      field: "email",
      text: `The Email Is'nt In Correct Format`,
    });
  }
  if (user.password.length < 7) {
    errorMessages.push({
      field: "password",
      text: `The Password Must Be 7 letters or more`,
    });
  }
  return errorMessages;
};

export { userFormValidation };
