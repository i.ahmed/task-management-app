import validator from "validator";

export const taskValidation = (task) => {
  const errors = [];
  if (validator.isEmpty(task.description)) {
    errors.push({ text: "Description Field Is Required" });
  }
  return errors;
};
