import React, { useContext, useState, useEffect } from "react";
import * as User from "./actions/user";
import * as Task from "./actions/task";

const AppContext = React.createContext();
const AppProvider = ({ children }) => {
  const [currentUser, setCurrentUser] = useState({ _id: "", token: "" });
  const [isError, setIsError] = useState(false);
  const [errors, setErrors] = useState([]);
  const [tasks, setTasks] = useState([]);
  useEffect(() => {
    const user = localStorage.getItem("currentUser");
    if (user) {
      setCurrentUser(JSON.parse(user));
    }
  }, []);
  return (
    <AppContext.Provider
      value={{
        login: User.login,
        logout: User.logout,
        signup: User.signup,
        getTasks: Task.getTasks,
        getTask: Task.getTask,
        addTask: Task.addTask,
        updateTask: Task.updateTask,
        deleteTask: Task.deleteTask,
        currentUser,
        setCurrentUser,
        isError,
        setIsError,
        errors,
        setErrors,
        tasks,
        setTasks,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

const useGlobalContext = () => {
  return useContext(AppContext);
};

export { AppProvider, useGlobalContext };
