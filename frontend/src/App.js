import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Login from "./componants/Login";
import Signup from "./componants/SignUp";
import Tasks from "./componants/Tasks";
import TaskForm from "./componants/TaskForm";
import { useGlobalContext } from "./context";
const App = () => {
  const {
    currentUser: { _id },
  } = useGlobalContext();
  return (
    <Router>
      <Switch>
        <Route
          exact
          path="/"
          render={() => (!_id ? <Login /> : <Redirect to="/tasks" />)}
        />
        <Route
          path="/signup"
          render={() => (!_id ? <Signup /> : <Redirect to="/tasks" />)}
        />
        <Route
          path="/tasks"
          render={() => (!_id ? <Redirect to="/" /> : <Tasks />)}
        />
        <Route
          path="/task/add"
          render={(props) =>
            !_id ? <Redirect to="/" /> : <TaskForm {...props} />
          }
        />
        <Route
          path="/task/:id/edit"
          render={(props) =>
            !_id ? <Redirect to="/" /> : <TaskForm {...props} />
          }
        />
      </Switch>
    </Router>
  );
};

export default App;
