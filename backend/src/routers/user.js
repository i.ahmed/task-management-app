const express = require('express');
const auth = require('../middlewares/auth');
const User = require('../models/user');
const { sendWelcomEmail } = require('../emails/accounts');
const router = new express.Router();
const GOOGLE_PROVIDER = 'google';
const FACEBOOK_PROVIDER = 'facebook';
router.post('/users',async (req,res) =>{
    try{
        const doublcateUser = await User.findUserByEmail(req.body.email || '');
        if(!doublcateUser) {
            const user = new User(req.body);
            await user.save();
            sendWelcomEmail(user.email,user.name);
            const token = await user.createAuthToken();
            return res.status(201).send({user,token});
        }
        throw Error('This Email Already Registered');
    }catch(e){
        res.status(400).send({error: e.message});
    }
});

router.post('/users/login', async (req,res)=> {
    try {
        const user = await User.findByCredintals(req.body.email,req.body.password);
        const token = await user.createAuthToken();
        res.send({user,token});
    } catch (e) {
        res.status(404).send({error: e.message});
    }
});

router.post('/users/login/google', async (req,res)=> {
    try {
        const user = await User.findByProvider(req.body.email, GOOGLE_PROVIDER);
        const token = await user.createAuthToken();
        res.send({user,token});
    } catch (e) {
        res.status(404).send({error: e.message});
    }
});

router.post('/users/login/facebook', async (req,res)=> {
    try {
         const user = await User.findByProvider(req.body.email, FACEBOOK_PROVIDER);
        const token = await user.createAuthToken();
        res.send({user,token});
    } catch (e) {
        res.status(404).send({error: e.message});
    }
});

router.post('/users/logout',auth ,async (req,res)=>{
    try{
        req.user.tokens = req.user.tokens.filter(token=>{
            return token.token !== req.token;
        });
        await req.user.save();
        res.send({message:'logged out successfully'});
    }catch(e){
        res.status(500).send(e);
    }
});
module.exports = router;
