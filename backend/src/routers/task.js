const express = require('express');
const mongoose = require('mongoose');
const auth = require('../middlewares/auth');
const Task = require('../models/task');
const router = new express.Router;

router.post('/tasks', auth,async (req,res) =>{
    const task = new Task({
        ...req.body,
        owner:req.user._id
    });
    try{
        await task.save();
        res.status(201).send(task);
    }catch(e){
        res.status(400).send(e);
    }
});

router.get('/tasks',auth,async (req,res)=>{
    try{
       await req.user.populate({
           path:'tasks'
       }).execPopulate();
       res.send(req.user.tasks);
    }catch(e){
        res.status(500).send(e);
    }
});


router.get('/tasks/:id',auth,async (req,res)=>{
    if(mongoose.Types.ObjectId.isValid(req.params.id)){
        const _id = req.params.id;
        try{
            const task = await Task.findOne({_id,owner:req.user._id});
            if(!task) return res.status(404).send();
            res.send(task);
        }catch(e){
            res.status(500).send(e);
        }
    }else{
        res.status(400).send({error:'This Id is Invalid'});
    }
});

router.patch('/tasks/:id',auth,async (req,res)=>{
    const updates = Object.keys(req.body);
    const allowedUpdates = ['completed','description'];
    const isValidOpretion = updates.every(update => allowedUpdates.includes(update));
    if(!isValidOpretion) return res.status(400).send({error: "Updates Invalid"});

    if(mongoose.Types.ObjectId.isValid(req.params.id)){
        const _id = req.params.id;
        try {
            const task = await Task.findOne({ _id,owner:req.user._id});
            if(!task) return res.status(404).send();
            updates.forEach(update => task[update] = req.body[update]);
            await task.save();
            res.send(task);
        } catch (e) {
            res.status(400).send(e);
        }
        
    }else{
        res.status(400).send({error:"This Id Is Invalid"});
    }
});

router.delete('/tasks/:id',auth,async (req,res)=>{
    if(mongoose.Types.ObjectId.isValid(req.params.id)){
        try {
            const _id = req.params.id;
            const task = await Task.findOneAndDelete({_id,owner:req.user._id});
            if(!task) return res.status(404).send();
            res.send(task);
        } catch (e) {
            res.status(500).send(e);
        }
    }else{
        res.status(400).send({error:"This Id Is Invalid"});
    }
});

module.exports = router;