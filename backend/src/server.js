const express = require('express');
const cors = require('cors');
const userRouter = require('./routers/user');
const taskRouter = require('./routers/task');
require('./db/mongoose');

const app = express();
app.use(express.json());
app.use(cors());
app.use('/api',userRouter);
app.use('/api',taskRouter);
module.exports = app;
