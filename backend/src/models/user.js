const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Task = require('./task');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required:true,
        lowercase:true,
        trim:true
    },
    email:{
        type: String,
        requried: true,
        lowercase: true,
        trim:true,
        unique: true
    },
    password:{
        type: String,
        minlength: 7,
        trim: true
    },
    provider:{
        type: String,
        trim: true,
        lowercase: true
    },
    tokens:[{
        token:{
            type: String,
            required: true
        }
    }]
},{
    timestamps:true
});

userSchema.virtual('tasks',{
    ref:'Task',
    localField:'_id',
    foreignField:'owner'
});

userSchema.methods.toJSON = function(){
    const user = this;
    const userObject = user.toObject();
    delete userObject.password;
    delete userObject.tokens;
    delete userObject.avatar;
    return userObject;
}

userSchema.methods.createAuthToken = async function(){
    const user = this;
    const token = await jwt.sign({_id:user._id.toString()},process.env.JWT_SECRET);
    user.tokens = user.tokens.concat({token})
    await user.save();
    return token;
}

userSchema.statics.findByCredintals = async (email,password)=>{
    const user = await User.findOne({email});
    if(!user) throw new Error("Unable To Login In");
    const isMatch = await bcrypt.compare(password,user.password);
    if(!isMatch) throw new Error("Unable To Login In");

    return user;
}

userSchema.statics.findByProvider = async (email, provider) => {
    const user = await User.findOne({email, provider});
    if(!user) throw new Error("Unable To Login With This Account");
    return user;
}

userSchema.statics.findUserByEmail = async (email) => {
    const user = await User.findOne({email});
    return user;
}
userSchema.pre('save',async function(next){
    const user = this;
    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password,8);
    }
    next();
});

userSchema.pre('remove',async function(next){
    const user = this;
    await Task.deleteMany({owner:user._id});
    next();
});

const User = mongoose.model('User',userSchema);

module.exports = User;