const request = require('supertest');
const app = require('../src/server');
const User = require('../src/models/user');
const {userOne,userOneId,setupDatabase} = require('./fixtures/db');

beforeEach(setupDatabase);

test('Should add new user', async () => {
    const response = await request(app).post('/api/users').send({
        name:'salim ahmed',
        email:'test@gmail.com',
        password:'ibrahim123'
    }).expect(201);

    const user = await User.findById(response.body.user._id);
    expect(user).not.toBeNull();

    expect(response.body).toMatchObject({
        user:{
            name:'salim ahmed',
            email:'test@gmail.com'
        },
        token:user.tokens[0].token
    });
});

test('Should login existing user', async () => {
    const response = await request(app).post('/api/users/login').send({
        email:userOne.email,
        password:userOne.password
    }).expect(200);

    const user = await User.findById(userOneId);
    expect(response.body.token).toBe(user.tokens[1].token);
});

test('Should not login unexistent user', async () => {
    await request(app).post('/api/users/login').send({
        email:'hellow@hellow.com',
        password:'hellow123'
    }).expect(404);
});

