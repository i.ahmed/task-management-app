const request = require('supertest');
const app = require('../src/server');
const Task = require('../src/models/task');
const {
    userOne,
    userOneId,
    userTwo,
    taskOne,
    setupDatabase,
    userTwoId,
    taskTwo,
    taskThree } = require('./fixtures/db');

beforeEach(setupDatabase);

test('Should create new task', async () => {
    const response = await request(app)
        .post('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({description: 'Hellow From Test'})
        .expect(201);
    const task = await Task.findById(response.body._id);
    expect(task).not.toBeNull();
    expect(task.completed).toEqual(false);    
});

test('Should get user tasks', async () => {
    const response = await request(app)
        .get('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200);

      expect(response.body.length).toEqual(2);  
});

test('Should not allow user to delete ather users tasks', async () => {
    await request(app)
        .delete(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
        .send()
        .expect(404);
    
    const task = await Task.findById(taskOne._id);
    expect(task).not.toBeNull();
});