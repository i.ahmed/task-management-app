const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const User = require('../../src/models/user');
const Task  = require('../../src/models/task');
const userOneId = new mongoose.Types.ObjectId();
const userOne = {
    _id:userOneId,
    name:'test123',
    email:'test123@example.com',
    password:'test1234',
    tokens:[{
        token: jwt.sign({ _id: userOneId},process.env.JWT_SECRET)
    }]
}

const userTwoId = new mongoose.Types.ObjectId();
const userTwo = {
    _id: userTwoId,
    name: 'salim ahmed',
    email:'salim@example.com',
    password: 'salim1234!!',
    tokens:[{
        token: jwt.sign({_id:userTwoId},process.env.JWT_SECRET)
    }]
}

const taskOne = {
    _id: new mongoose.Types.ObjectId(),
    description:'First Task',
    completed:false,
    owner: userOneId
}
const taskTwo = {
    _id: new mongoose.Types.ObjectId(),
    description:'Secand Task',
    completed:true,
    owner: userOneId
}
const taskThree = {
    _id: new mongoose.Types.ObjectId(),
    description:'Secand Third',
    completed:true,
    owner: userTwoId
}
const setupDatabase = async () => {
    await User.deleteMany();
    await Task.deleteMany();
    await new User(userOne).save();
    await new User(userTwo).save();
    await new Task(taskOne).save();
    await new Task(taskTwo).save();
    await new Task(taskThree).save();
}

module.exports = {
    userOne,
    userOneId,
    userTwoId,
    setupDatabase,
    taskOne,
    taskTwo,
    taskThree,
    userTwo
}